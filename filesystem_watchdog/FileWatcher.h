#pragma once
#include "pch.h"

enum class FileStatus { created, modified, erased };

class FileWatcher
{
public:
	FileWatcher(std::string path_to_watch, std::chrono::duration<int, std::milli> delay);
	~FileWatcher();
	void start(const std::function<void(std::string, FileStatus)>& action);
	bool contains(const std::string& key);

private:
	std::string path_to_watch;
	std::chrono::duration<int, std::milli> delay; // time interval to check for changes in directories
	std::unordered_map<std::string, std::filesystem::file_time_type> _paths;
	bool _running = true;
};




