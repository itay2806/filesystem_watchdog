#include "FileWatcher.h"

/**
 * \brief C'tor of the fileWatcher class
 * \param path_to_watch directory path to check for changes
 * \param delay time interval
 */
FileWatcher::FileWatcher(std::string path_to_watch, std::chrono::duration<int, std::milli> delay): path_to_watch(path_to_watch), delay(delay)
{
	for (auto& file : std::filesystem::recursive_directory_iterator(path_to_watch))
	{
		_paths[file.path().string()] = std::filesystem::last_write_time(file);
	}
}

/**
 * \brief D'tor 
 */
FileWatcher::~FileWatcher()
{
}
/**
 * \brief start watching for changes in directory in case of change execute user function
 * \param action 
 */
void FileWatcher::start(const std::function<void(std::string, FileStatus)>& action)
{
	while (_running)
	{
		std::this_thread::sleep_for(delay);
		// check if file erased
		for (auto& file_path : _paths)
		{
			if (!std::filesystem::exists(file_path.first))
			{
				action(file_path.first, FileStatus::erased);
				_paths.erase(file_path.first); // erase by key(file name)
			}
		}
	}

	// check if file created or modified
	for (auto& file : std::filesystem::recursive_directory_iterator(path_to_watch))
	{
		auto current_file_last_write_time = std::filesystem::last_write_time(file);
		// file creation
		if (!contains(file.path().string())) {
			_paths[file.path().string()] = current_file_last_write_time;
			action(file.path().string(), FileStatus::created);
		}
		// file modification
		else
		{
			if(_paths[file.path().string()] != current_file_last_write_time)
			{
				_paths[file.path().string()] = current_file_last_write_time;
				action(file.path().string(), FileStatus::modified);
			}
		}
	}
}

/**
 * \brief check if directory contains file
 * \param key 
 * \return file exists or not
 */
bool FileWatcher::contains(const std::string& key)
{
	return _paths.find(key) != _paths.end();
}
